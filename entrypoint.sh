#!/bin/bash

echo "--- INIT ---"

# load web server on port 4242
python3 /app/app.py &

# wait for pod kill
touch /tmp/.wait.pod
tail -f /tmp/.wait.pod

echo "=== END ==="
