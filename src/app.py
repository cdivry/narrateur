#!/usr/bin/env python3

import bs4
import flask
import os
import random
import string
import subprocess

##########################################
# TOOLS                                  #
##########################################
def get_random_string(length : int):
    letters = string.ascii_letters + "0123456789"
    return ''.join(random.choice(letters) for i in range(length))

def get_content_opf_filename(dossier : str):
    for root, dirs, fichiers in os.walk(dossier):
        if "content.opf" in fichiers:
            return os.path.join(root, "content.opf")
    return None

def get_content_pages(content_opf_filename : str):
    print(f"get_content_pages: {content_opf_filename}")
    if content_opf_filename is not None:
        with open(content_opf_filename, "r") as fd:
            xml_raw = fd.read()
            soup = bs4.BeautifulSoup(xml_raw, features="xml")
            elements = soup.find("manifest").findAll("item", {"media-type": "application/xhtml+xml"})
            # print([item["href"] for item in elements])
            return [item["href"] for item in elements]
    return []

def get_page_text(page_filename : str):
    print(f"get_page_text: {page_filename}")
    with open(page_filename, "r") as fd:
        html_raw = fd.read()
        soup = bs4.BeautifulSoup(html_raw, "lxml")
        texte = soup.find("body").getText()
        return texte
    return ""

def extraction_epub(filename : str):
    unique_id = get_random_string(42)
    dossier = f"data/extraction_{unique_id}"
    if not os.path.isdir(dossier):
        os.mkdir(dossier)
    # extract 
    unzip_cmd = [
        "unzip",
        filename,
        "-d", dossier, # destination
    ]
    print(unzip_cmd)
    pid = subprocess.run(unzip_cmd, capture_output=False)
    # read content.opf
    content_opf_file = get_content_opf_filename(dossier)
    dossier = "/".join(content_opf_file.split("/")[:-1:])
    pages = get_content_pages(content_opf_file)
    texte = ""
    for page in pages:
        texte += get_page_text(f"{dossier}/{page}")
        texte += "\n\n\n\n\n\n\n\n\n\n" # delai de lecture
    return texte


def convert_to_wav(texte : str):
    unique_id = get_random_string(42)
    wav_file = f"./data/{unique_id}.wav"
    txt_file = f"./data/{unique_id}.txt"
    with open(txt_file, "w") as fd:
        fd.write(texte)
    print(f"generating WAV file: {wav_file}")
    espeak_cmd = [
        "espeak-ng",
        "-v", "mb/mb-fr3", # voix
        "-s", "120", # vitesse
        "-f", txt_file, # fichier de texte à lire
        "--stdout",
    ]
    pid = subprocess.run(espeak_cmd, capture_output=True)
    with open(wav_file, "wb") as fd:
        fd.write(pid.stdout)
    return wav_file

##########################################
# ROUTES                                 #
##########################################


app = flask.Flask(__name__)

@app.route('/lire/texte', methods=['POST'])
def post_lire_texte():
    texte = flask.request.form["texte"]  # data is empty
    try:
        print(texte)
        path_to_file = convert_to_wav(texte)
        print(path_to_file)
        return flask.send_file(
            path_to_file, 
            mimetype="audio/wav", 
            as_attachment=True, 
            download_name="narration.wav"
        )
    except Exception as e:
        return flask.make_response(
            {"err": "Bad Request", "debug": str(e)},
            400
        )

    return flask.make_response(
        "OK",
        200
    )

@app.route('/lire/texte', methods=['GET'])
def get_lire_texte():
    html = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>Narrateur</title>
        </head>
        <body>
            <form method="POST" action="/lire/texte">
                <label for="file">Texte à lire :</label>
                <br />
                <textarea id="texte" name="texte" rows="5" /></textarea>
                <br />
                <input type="submit" value="Lire le texte" />
            </form>
        </body>
    </html>
    """
    return flask.make_response(
        html,
        200
    )


@app.route('/lire/epub', methods=['POST'])
def post_lire_epub():
    fichier = flask.request.files.get('fichier')
    if fichier.content_type != "application/epub+zip":
       return flask.make_response(
        f"Mauvais mimetype: {fichier.content_type}",
        200
    )
    fichier.save(fichier.filename) # ecris le livre sur le disque

    texte = extraction_epub(fichier.filename)
    #try:
    if True:
        path_to_file = convert_to_wav(texte)
        wav_filename = fichier.filename.replace(".epub", ".wav")
        return flask.send_file(
            path_to_file, 
            mimetype="audio/wav", 
            as_attachment=True, 
            download_name=wav_filename
        )
    # except Exception as e:
    #     return flask.make_response(
    #         {"err": "Bad Request"},
    #         400
    #     )

@app.route('/lire/epub', methods=['GET'])
def get_lire_epub():
    html = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>Narrateur</title>
        </head>
        <body>
            <form method="POST" action="/lire/epub" enctype="multipart/form-data">
                <label for="file">Fichier livre (.epub):</label>
                <input type="file" name="fichier" accept=".epub" />
                <br />
                <input type="submit" value="Lire le livre" />
            </form>
        </body>
    </html>
    """
    return flask.make_response(
        html,
        200
    )


@app.route('/', methods=['GET'])
def root():
    html = """
    <a href="/lire/texte">- Lire un texte</a><br />
    <a href="/lire/epub">- Lire un livre .epub</a><br />
    """
    return flask.make_response(
        html,
        200
    )



##########################################
# ENTRYPOINT                             #
##########################################

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='4242', debug=True)
