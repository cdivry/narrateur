FROM debian:bookworm

RUN mkdir -pv /app

RUN sed -i -e's/main/main\ contrib\ non-free/g' /etc/apt/sources.list.d/debian.sources

RUN apt update
RUN apt install -y \
	python3-dev \
	python3-pip \
	python3-venv \
	python3-bs4 \
	python3-flask \
	pulseaudio \
	apulse \
	espeak-ng \
	espeak-ng-data \
	mbrola \
	mbrola-fr3 \
	speech-dispatcher \
	speech-dispatcher-espeak-ng \
	unzip \
	sudo


COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
#ENTRYPOINT /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]


COPY ./src/app.py /app/app.py
RUN chmod +x /app/app.py


# Set user/resu
RUN useradd -ms /bin/bash --password 6HDJXfMtH6sCY user
RUN adduser user sudo
RUN adduser user sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER user
WORKDIR /app

